import json

from cp4_ai.models import Board
from cp4_ai.strategy.minmax import AdvancedStrategy


def test_minmax():
    s = '000000 000000 000000 h00000 000000 000000 000000'.replace(' ', '')
    board = Board(s)

    strategy = AdvancedStrategy()
    result = strategy.get_move(board)

    print("minmax:", result)

    # print(json.dumps(state.as_dict(), indent=2))
    # for n, child in enumerate(state.next_states):
    #     print(f"#{n}:")
    #     print(child.board.to_display())
